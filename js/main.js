class GameItem {
    constructor(name, xPosition = 0, yPosition = 0) {
        this._name = name;
        this._xPos = xPosition;
        this._yPos = yPosition;
    }
    set xPos(xPosition) {
        this._xPos = xPosition;
    }
    set yPos(yPosition) {
        this._yPos = yPosition;
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
        const image = document.createElement('img');
        image.src = `./assets/${this._name}.png `;
        this._element.appendChild(image);
        container.appendChild(this._element);
    }
    update() {
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
    }
}
class car extends GameItem {
    constructor(name, xPosition = 0, yPosition = 0) {
        super(name, xPosition, yPosition);
    }
    move(xPosition) {
        this._xPos -= xPosition;
        this._element.classList.add('driving');
    }
}
class Game {
    constructor() {
        this._element = document.getElementById('container');
        this.keyDownHandler = (e) => {
            if (e.keyCode === 32) {
                this._car.move(50);
                this.update();
            }
        };
        this._car = new car('car');
        this._parkingspot = new parkingspot('parkingspot', 0);
        this._scoreboard = new Scoreboard('scoreboard');
        window.addEventListener('keydown', this.keyDownHandler);
        this.draw();
    }
    collision() {
        const parkingspotRect = document.getElementById('parkingspot-0').getBoundingClientRect();
        const carRect = document.getElementById('car').getBoundingClientRect();
        if (parkingspotRect.right >= carRect.left) {
            window.removeEventListener('keydown', this.keyDownHandler);
            this._scoreboard.addScore();
        }
    }
    draw() {
        this._car.draw(this._element);
        this._parkingspot.draw(this._element);
        this._scoreboard.draw(this._element);
    }
    update() {
        this.collision();
        this._car.update();
        this._parkingspot.update();
        this._scoreboard.update();
    }
}
let app;
(function () {
    let init = function () {
        app = new Game();
    };
    window.addEventListener('load', init);
})();
class parkingspot extends GameItem {
    constructor(name, id, xPosition = 0, yPosition = 0) {
        super(name, xPosition, yPosition);
        this._id = id;
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = `${this._name}-${this._id}`;
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
        const image = document.createElement('img');
        image.src = `./assets/${this._name}.png `;
        this._element.appendChild(image);
        container.appendChild(this._element);
    }
    remove(container) {
        const elem = document.getElementById(`${this._name}-${this._id}`);
        container.removeChild(elem);
    }
}
class Scoreboard extends GameItem {
    constructor(name) {
        super(name);
        this._score = 0;
    }
    get score() {
        return this._score;
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        const p = document.createElement('p');
        p.innerHTML = 'The score is: ';
        const span = document.createElement('span');
        span.innerHTML = this._score.toString();
        p.appendChild(span);
        this._element.appendChild(p);
        container.appendChild(this._element);
    }
    update() {
        const scoreSpan = this._element.childNodes[0].childNodes[1];
        scoreSpan.innerHTML = this._score.toString();
    }
    addScore() {
        this._score += 1;
    }
}
//# sourceMappingURL=main.js.map