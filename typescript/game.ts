class Game {
    //attr
    private _element: HTMLElement = document.getElementById('container');
    private _car: car;
    private _parkingspot: parkingspot; //use an array if you have multiple gameItems of the same sort
    private _scoreboard: Scoreboard;
  
    /**
     * Create the Game class
     */
    constructor() {
      //create some gameItems
      this._car = new car('car');
      this._parkingspot = new parkingspot('parkingspot', 0);
      this._scoreboard = new Scoreboard('scoreboard');

      //add keydown handler to the window object
      window.addEventListener('keydown', this.keyDownHandler);
  
      //draw is initial state
      this.draw();
    }
  
    /**
     * Function to detect collision between two objects
     */
    public collision(): void {
      //use elem.getBoundingClientRect() for getting the wright coordinates and measurements of the element
      const parkingspotRect = document.getElementById('parkingspot-0').getBoundingClientRect();
      const carRect = document.getElementById('car').getBoundingClientRect();
  
      if (parkingspotRect.right >= carRect.left) {
        window.removeEventListener('keydown', this.keyDownHandler);
        this._scoreboard.addScore();
    }
  }
    /**
     * Function to draw the initial state of al living objects
     */
    public draw(): void {
      this._car.draw(this._element);
      this._parkingspot.draw(this._element);
      this._scoreboard.draw(this._element);
    }
  
    /**
     * Function to update the state of all living objects
     */
    public update(): void { //function formerly known as render()
      this.collision();
      this._car.update();
      this._parkingspot.update();
      this._scoreboard.update();
    }
  
    /**
     * Function to handle the keyboard event
     * @param {KeyboardEvent} - event
     */
    public keyDownHandler = (e: KeyboardEvent): void => {
      if (e.keyCode === 32) {
        
        //move buzz 50px
        this._car.move(50);
        this.update();

      }
    }
  }